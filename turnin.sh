#!/bin/bash

git pull origin master
git add --all
read -p "Enter commit message: " msg
git commit -m "${msg}"
git push origin master

################
# INSTRUCTIONS #
################

# 1) use command: chmod +x turnin.sh
#    to make this executable.

# Record your credentials so you don't have to enter your password each time

# 2) git config --local user.name "your name"
# 3) git config --local user.email "your email"
# 4) to use, type: ./turnin.sh


