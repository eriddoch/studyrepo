"""

Test file for QR Decomposition

"""

import qr_decomposition as qr
import pytest
import numpy as np
from scipy import linalg
import sympy as sy

@pytest.fixture
def set_up_matrices():
    # create test matrix:

    A = sy.Matrix([
        [1,2,3],
        [4,5,6],
        [7,1,1]
    ])

    B = np.array([
        [5,9,33],
        [6,9,62],
        [1,2,91]
    ])

    return A, B

def test_qr_gram_schidt(set_up_matrices):
    A = set_up_matrices[0]
        
    # assert np.allclose(A, B)

    Q, R = qr.qr_gram_schmidt(A)
    assert np.allclose(Q @ R, A)

def test_abs_det(set_up_matrices):
    A, B = set_up_matrices
    det = abs(sy.Matrix(A).det())
    assert qr.abs_det(A) == det
                      

# def test_qr_gram_schidt(set_up_matrices):
#     A = set_up_matrices
#     assert qr.qr_gram_schmidt(A) == linalg.qr(A)
