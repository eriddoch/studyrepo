"""

Test File for BFS

"""

from collections import deque
import pytest
import breadth_first_search as bfs

def test_Graph():
	A = bfs.Graph()
	A.add_edge('a','b')
	A.add_edge('a','c')
	A.add_edge('a','d')
	A.add_edge('b','e')
	A.add_edge('b','f')
	A.add_edge('c','g')
	A.add_edge('c','h')
	A.add_edge('c','i')
	A.add_edge('c','g')
	A.add_edge('c','k')
	assert A.d["a"] == {'b','c','d'}

def test_remove_node():
	A = bfs.Graph()
	A.add_node("A")
	A.add_node("B")
	A.add_node("C")
	A.add_node("D")
	A.add_edge("A","B")
	A.add_edge("D","A")
	A.add_edge("D","B")
	A.add_edge("D","C")
	assert A.d["D"] == {'A','B','C'}

	A.remove_node("A")
	assert A.d["D"] == {'B','C'}

	A.remove_edge("D","B")
	assert A.d["D"] == {'C'}
	assert("A" in A.d["D"]) == False

	with pytest.raises(KeyError) as excinfo:
		A.remove_node("poop")

	with pytest.raises(KeyError) as excinfo:
		A.remove_edge("p","w")

def test_traverse():
	A = bfs.Graph()
	A.add_node("A")
	A.add_node("B")
	A.add_node("C")
	A.add_node("D")
	A.add_edge("A","B")
	A.add_edge("D","A")
	A.add_edge("D","B")
	A.add_edge("D","C")
	print(A)
	assert A.traverse('A') == ['A','B','D','C']
	print(A.traverse("A"))
	
def test_remove_edge():
	A = bfs.Graph()
	A.add_edge('a','b')
	A.add_edge('a','c')
	A.remove_node('c') 
	assert str(A) == "{'a': {'b'}, 'b': {'a'}}"

def test_shortest_path():
	A = bfs.Graph()
	A.add_node("A")
	A.add_node("B")
	A.add_node("C")
	A.add_node("D")
	A.add_edge("A","B")
	A.add_edge("D","A")
	A.add_edge("D","B")
	A.add_edge("D","C")
	target = "C"
	source = "A"
	assert ["A", "D", "C"] == A.shortest_path(source, target)

	B = bfs.Graph()
	B.add_node("A")
	B.add_node("B")
	B.add_node("C")
	B.add_node("D")
	B.add_node("E")
	B.add_node("F")
	B.add_edge("A","B")
	B.add_edge("D","A")
	B.add_edge("D","B")
	B.add_edge("D","C")
	B.add_edge("C","E")
	B.add_edge("E","F")
	B.add_edge("A","F")
	target = "F"
	source = "A"
	assert ["A", "F"] == B.shortest_path(source, target)
	with pytest.raises(KeyError) as excinfo:
		B.shortest_path("Z", "A")
	with pytest.raises(KeyError) as excinfo:
		B.shortest_path("A", "Z")




