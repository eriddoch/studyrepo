'''

Image Segmentation Test File

'''

import numpy as np
import image_segmentation as img
import pytest
from scipy.sparse import csgraph
from scipy import linalg as la
import sympy as sy


@pytest.fixture
def set_up_adjacency():
    ''' 
    Adjacency matrixes for G1 and G2 from the book
    '''

    A1 = np.array([ [0,1,0,0,1,1],
                    [1,0,1,0,1,0],
                    [0,1,0,1,0,0],
                    [0,0,1,0,1,1],
                    [1,1,0,1,0,0],
                    [1,0,0,1,0,0]])

    A2 = np.array([[0,3,0,0,0,0],
                  [3,0,0,0,0,0],
                  [0,0,0,1,0,0],
                  [0,0,1,0,2,.5],
                  [0,0,0,2,0,1],
                  [0,0,0,.5,1,0] ])

    return A1, A2

def test_lapacian(set_up_adjacency):
    # Check that our results match with
    # scipy.sparse.csgraph.lapacian()

    A1, A2 = set_up_adjacency
    
    # Laplacian matrix of G1:
    L1 = img.laplacian(A1)
    # Scypy Laplacian matrix of G1:
    scy_L1 = csgraph.laplacian(A1)

    assert np.allclose(L1, scy_L1)


    # Laplacian matrix of G2:
    L2 = img.laplacian(A2)
    # Scypy Laplacian matrix of G2:
    scy_L2 = csgraph.laplacian(A2)

    assert np.allclose(L2, scy_L2)


"""

Unit Test File For:
Image Segmentation Lab

"""

@pytest.fixture
def set_up_matrices():

    G_1 = np.array(
        [
        [0, 1, 0, 0, 1, 1],
        [1, 0, 1, 0, 1, 0], 
        [0, 1, 0, 1, 0, 0],
        [0, 0, 1, 0, 1, 1],
        [1, 1, 0, 1, 0, 0],
        [1, 0, 0, 1, 0, 0]
        ]
    )

    G_2 = np.array(
        [[0,3,0,0,0,0],
        [3,0,0,0,0,0],
        [0,0,0,1,0,0],
        [0,0,1,0,2,.5],
        [0,0,0,2,0,1],
        [0,0,0,.5,1,0]])   

    return G_1, G_2 

def test_laplacian(set_up_matrices):
    G_1, G_2 = set_up_matrices

    # Test: was the Laplacian calculated correctly?
    assert np.allclose( csgraph.laplacian(G_1), img.laplacian(G_1) )
    
    # Test 2: check G_2 from volume 1 labs
    assert np.allclose( csgraph.laplacian(G_2), img.laplacian(G_2) )


    
def test_connected_components(set_up_matrices):
    G_1, G_2 = set_up_matrices

    """
    
    Graph. Observe that there are 2 connected regions:
    1 0 1
    1 0 1
    0 0 1
    
    """
    # Adjacency matrix of above graph
    # Joe_Eric_Matrix = np.array([
    #     [0, 0, 0, 1, 0, 0, 0, 0, 0],
    #     [0, 0, 0, 0, 0, 0, 0, 0, 0],
    #     [0, 0, 0, 0, 0, 1, 0, 0, 0],
    #     [1, 0, 0, 0, 0, 0, 0, 0, 0],
    #     [0, 0, 0, 0, 0, 0, 0, 0, 0],
    #     [0, 0, 1, 0, 0, 0, 0, 0, 1],
    #     [0, 0, 0, 0, 0, 0, 0, 0, 0],
    #     [0, 0, 0, 0, 0, 0, 0, 0, 0],
    #     [0, 0, 0, 0, 0, 1, 0, 0, 0]
    # ])

    Joe_Eric_Matrix = np.array([
        [0, 0, 1, 0, 0],
        [0, 0, 0, 1, 0],
        [1, 0, 0, 0, 0],
        [0, 1, 0, 0, 1],
        [0, 0, 0, 1, 0]
    ])

    # Test G_1
    connected_components, multiplicity = img.connectivity(G_1)
    assert ( connected_components == csgraph.connected_components((G_1))[0]
        and multiplicity > 0 )

    # Test G_2
    connected_components, multiplicity = img.connectivity(G_2)
    assert ( connected_components == csgraph.connected_components((G_2))[0]
        and multiplicity > 0 )

    # SUPER TEST! The Joe_Eric_Matrix: How many islands are there?
    connected_components, multiplicity = img.connectivity(Joe_Eric_Matrix)
    assert ( connected_components == 2 and multiplicity == 0 )

    # assert False

def test_ImageSegmenter():
    # test __init__()
    image = img.ImageSegmenter("dream.png")
    # image.show_original()
    # test gray image
    gray_image = img.ImageSegmenter("dream_gray.png")
    # gray_image.show_original()
    #image.show_gray()
    
    # Test: Dimension of brightness == 1
    assert len(image.brightness.shape) == 1

    image.segment()

    assert False

def test_adjacency():
    image = img.ImageSegmenter()
    image.adjacency()
    assert False


    
    

